import { args, os } from "./config.ts";
import { appVer, Command, Select } from "./deps.ts";
import { main as clearDir } from "./clear_dir.ts";
import { main as clearSplit } from "./clear_split_cocs.ts";
import { main as copyCocs } from "./copy_cocs.ts";
import { main as createMocks } from "./create_mocks.ts";
import { main as hideLogo } from "./hide_logo.ts";
import { main as indexFiles } from "./index_files.ts";
import { main as markCocs } from "./mark_cocs.ts";
import { main as mergeCocs } from "./merge_cocs.ts";
import { main as splitCocs } from "./split_cocs.ts";
import { dlog, intro } from "./utils.ts";

async function main() {
  if (!args.V) intro();
  await new Command()
    // Main command.
    .name("coc")
    .version(appVer).meta("deno", Deno.version.deno)
    .description(`Narzędzie do wyszukiwania dokumentów CoC
Zgłasznie błędów: https://bitbucket.org/jackfiszr/cocfinder/jira
© 2020-2022 Jacek Fiszer, MIT License`)
    .globalOption("--mock", "Run in mock mode/setup using mock data.")
    .globalOption(
      "--speak",
      "Informuj głosowo o wyniku działania programu (tylko Windows).",
    )
    .action(async function () {
      this.showHelp();
      await selectCmd();
    })
    // Mock command.
    .command("mock", "Utworzenie sztucznych plików wejściowych.")
    .action(createMocks)
    // Split command.
    .command("split", "Dzielenie nowych tranferów.")
    .option(
      "--check [liczba_dni]",
      "Sprawdzenie kompletności wcześniejszych dzieleń.",
    )
    .action(splitCocs)
    // Index command.
    .command("index", "Indeksowanie podzielonych CoC.")
    .action(indexFiles)
    // Splex command.
    .command("splex", "Dzielenie i indeksowanie w jednym.")
    .option(
      "--check [liczba_dni]",
      "Sprawdzenie kompletności wcześniejszych dzieleń.",
    )
    .action(async () => {
      await splitCocs();
      await indexFiles();
    })
    // Find command.
    .command("find [vin...]", "Wyszukiwanie CoC.")
    .option("--all", "Wyszukanie wszystkich wystąpień CoC.")
    .option("--no-index", "Przeszukanie systemu plików bez użycia indeksu.")
    .action(copyCocs)
    // Hide-logo command.
    .command("hide-logo", "Zasłonięcię logo marki.")
    .action(hideLogo)
    // Mark command.
    .command("mark", "Utworzenie kopii CoC z dopiskiem.")
    .option("-t, --text <tekst/liczba>", "Tekst, który ma być dodany do CoC.")
    .option("-f, --font-size <liczba:number>", "Wielkość czcionki.")
    .option(
      "-p, --pages <numery/odd/even>",
      "Strony, do których dopisać tekst.",
    )
    .option("-s, --skew <-360–360:number>", "Pochylenie tekstu w stopniach.")
    .option("-x, --coord-x <liczba:number>", "Pozycja tekstu względem osi X")
    .option("-y, --coord-y <liczba:number>", "Pozycja tekstu względem osi Y")
    .option(
      "-r, --red <0.0–1.0:number>",
      "Zawartość czerwieni w kolorze tekstu.",
    )
    .option(
      "-g, --green <0.0–1.0:number>",
      "Zawartość zieleni w kolorze tekstu.",
    )
    .option("-b, --blue <0.0–1.0>", "Zawartość niebieskiego w kolorze tekstu.")
    .option("-o, --opacity <0.0–1.0:number>", "Nieprzezroczystość tekstu.")
    .action(markCocs)
    // Merge command.
    .command("join", "Połączenie skopiowanych CoC.")
    .option("-s, --split", "Stare i nowe CoC w osobnych plikach.")
    .option("--hide-logo [marki]", "Zasłonięcię logo marki.")
    .action(mergeCocs)
    // Clear command.
    .command("clear", "Usunięcie skopiowanych CoC.")
    .action(clearDir)
    // Purge command.
    .command(
      "purge",
      "Usunięcie podzielonych CoC (działa tylko z opcją --mock).",
    )
    .action(clearSplit)
    .parse(Deno.args);
}

async function selectCmd() {
  const cmdStr = await Select.prompt({
    message: "Wybierz jedno z popularnych poleceń (↓d ↑u ⏎):",
    options: [
      { name: "coc find\t\t\t[ZNAJDŹ_COC]", value: "coc find" },
      {
        name: "coc find --all\t\t[ZNAJDŹ_COC_WSZYSTKIE_WYSTĄPIENIA]",
        value: "coc find --all",
      },
      { name: "coc join -s\t\t\t[ŁĄCZ_COC]", value: "coc join -s" },
      {
        name: "coc join --hide-logo -s\t[ŁĄCZ_COC_CHOWAJĄC_LOGO]",
        value: "coc join --hide-logo -s",
      },
      { name: "coc hide-logo\t\t[SCHOWAJ_LOGO.bat]", value: "coc hide-logo" },
      {
        name: "coc mark -p odd -x 750 -y 35\t[DUPLIKAT_COC]",
        value: "coc mark -p odd -x 750 -y 35",
      },
      { name: "coc clear\t\t\t[USUŃ_COC]", value: "coc clear" },
      { name: "coc splex\t\t\t[DZIEL_COC]", value: "coc splex" },
      {
        name: "coc splex --check 30\t\t[DZIEL_COC_Z_NAPRAWIANIEM]",
        value: "coc splex --check 30",
      },
      // Select.separator("----------------------------"),
      { name: "exit\t\t\t\t[WYJŚCIE]", value: "exit" },
    ],
  });
  if (cmdStr === "exit") return;
  try {
    await runCmd(cmdStr);
  } catch (err) {
    if (os === "windows") {
      runCmd(cmdStr.replace("coc", "coc.cmd"));
    } else {
      console.error(err);
    }
  }
}

async function runCmd(cmdStr: string) {
  dlog({
    color: "dim",
    title: "WYKONUJĘ",
    mainMsg: cmdStr,
  });
  const process = Deno.run({
    cmd: cmdStr.split(" "),
  });
  await process.status();
  process.close();
  dlog({
    color: "dim",
    title: "WYKONANO",
    mainMsg: cmdStr,
  });
}

if (import.meta.main) main();
