import { expandGlob, join, swissKnife } from "./deps.ts";
import { args, USER_DIR, USERNAME } from "./config.ts";

export async function main() {
  const globString = join(USER_DIR, "*.pdf");

  for await (const pdf of expandGlob(globString)) {
    await Deno.remove(pdf.path);
    console.log(`rm ${USERNAME}/${pdf.name}`);
    // if (args.speak) await swissKnife.speak("Ciap");
  }

  if (args.speak) await swissKnife.speak("Wszystko wytrute");
}

if (import.meta.main) main();
