import { join, parse } from "./deps.ts";
import { mapDisk, wmiFor } from "./utils.ts";

export const os = Deno.build.os;
export const args = parse(Deno.args, { string: ["user"] });

export const USERNAME = args["user"] || Deno.env.get("USERNAME") || "DEFAULT";
export const USER_DIR = join(Deno.cwd(), USERNAME);
export const VIN_LIST_FILE = join(Deno.cwd(), "lista_vin.txt");

export const USE_INDEX = !args["no-index"];
export const COPY_LAST = !args["all"];

const ROOT = "\\\\plvgppof001";
const DEPT = `${ROOT}\\DEPT`;
export const TRANSFERS = `${ROOT}\\TRANSFERS\\002_COC_VGP`;
export const HOMO_DIR_UNC = `${DEPT}\\CDOP\\DSG-CDOP\\Ds\\Homologacje`;
export const DISK_MAPPINGS = {
  K: DEPT,
  H: HOMO_DIR_UNC,
};
export const HOMO_DIR = mapDisk(HOMO_DIR_UNC, "K");
export const MOCK_DIR = "mocks";
export const TEMP_DIR = os === "linux" || args.mock
  ? join(MOCK_DIR, "tmp")
  : join(
    Deno.env.get("USERPROFILE") || ".",
    "AppData",
    "Local",
    "Temp",
    "cocfinder",
  );
export const SEARCH_DIR = os === "linux" || args.mock
  ? join(MOCK_DIR, "CoC_pdf")
  : join(HOMO_DIR, "CoC_pdf");
export const SOURCE_DIR = os === "linux" || args.mock
  ? join(MOCK_DIR, "source_data")
  : TRANSFERS;
export const MAIN_INDEX_FILE = join(SEARCH_DIR, "index.txt");
export const USER_INDEX_FILE = join(TEMP_DIR, "index.txt");
export const BRAND_DATA: {
  [brand: string]: {
    COM: string;
    PCL: string;
    PDF: string;
    OUT: string;
    WMI: string[];
    rotation: number;
  };
} = {
  "PORSCHE": {
    COM: "DEF.R36G60.COC.DATENN",
    PCL: "",
    PDF: "",
    OUT: "",
    WMI: wmiFor["PORSCHE"],
    rotation: 0,
  },
  "VW/AUDI": {
    COM: "DJB.R11G60.COM13",
    PCL: "",
    PDF: "DJB.R11G60.PDF",
    OUT: "VWAU",
    WMI: wmiFor["VW"].concat(wmiFor["AUDI"]),
    rotation: 270,
  },
  "SEAT": {
    COM: "DJB.R41G60.COM13",
    PCL: "DJB.R41G60.PCL5C",
    PDF: "MHV.N45G60.COCPDF",
    OUT: "SEAT",
    WMI: wmiFor["SEAT"],
    rotation: 90,
  },
  "SKODA": {
    COM: "DJT.R31G60.COM13",
    PCL: "",
    PDF: "DJT.R31G60.PDFC",
    OUT: "SKODA",
    WMI: wmiFor["SKODA"],
    rotation: 0,
  },
};
export const BRAND_DIRS = Object.keys(BRAND_DATA).map((brand) =>
  BRAND_DATA[brand].OUT
).filter((dir) => dir);
