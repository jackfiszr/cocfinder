import {
  basename,
  expandGlob,
  join,
  PDFDocument,
  swissKnife,
  writeAllSync,
} from "./deps.ts";
import { args, USER_DIR } from "./config.ts";
import {
  brandNames,
  cocToTxt,
  dlog,
  drawUpperRightRect,
  vinRegEx,
  wmiFor,
} from "./utils.ts";

const SPLIT = args.split || args.s;
const HIDE_LOGO = args["hide-logo"];
const HIDE_LOGO_FOR_WMI = typeof HIDE_LOGO === "string"
  ? HIDE_LOGO.split(",").map((brand) => wmiFor[brand.toUpperCase()]).flat()
  : [];

export async function main() {
  for await (const brand of brandNames) {
    const cocsByBrand = await getCocListForBrand(brand);
    const cocsToMerge = await splitToOldNewOrAll(cocsByBrand);
    for (const age of Object.keys(cocsToMerge)) {
      if (cocsToMerge[age as keyof typeof cocsToMerge].length > 0) {
        const pdfBytes = await mergeCocs(
          cocsToMerge[age as keyof typeof cocsToMerge],
        );
        const filename = `${age}_${brand}.pdf`;
        const filepath = join(USER_DIR, filename);
        await Deno.writeFile(filepath, pdfBytes);
        dlog({
          color: "green",
          title: brand,
          mainMsg: `${
            cocsToMerge[age as keyof typeof cocsToMerge].length
          } CoC zostało połączonych w pliku ${filepath}`,
        });
      }
    }
  }
  if (args.speak) swissKnife.speak("Wszyściutko pozłączyłam");
}

async function mergeCocs(cocsToMerge: string[]) {
  const mergedPdf = await PDFDocument.create();
  for (const pdfPath of cocsToMerge) {
    const vin = basename(pdfPath).substring(0, 17);
    const wmi = vin.substring(0, 3);
    const pdfBytes = await Deno.readFile(pdfPath);
    const pdfDoc = await PDFDocument.load(pdfBytes);
    if (pdfDoc.getPageCount() === 2) {
      const copiedPages = await mergedPdf.copyPages(
        pdfDoc,
        pdfDoc.getPageIndices(),
      );
      copiedPages.forEach((page, i) => {
        if (
          HIDE_LOGO && i % 2 === 0 &&
          (HIDE_LOGO_FOR_WMI.includes(wmi) || HIDE_LOGO_FOR_WMI.length === 0)
        ) {
          dlog({
            color: "yellow",
            title: vin,
            mainMsg: "chowam logo",
          });
          drawUpperRightRect(page);
        }
        mergedPdf.addPage(page);
      });
    }
  }
  const mergedPdfFile = await mergedPdf.save();
  return mergedPdfFile;
}

async function getCocListForBrand(brand: string) {
  const result: string[] = [];
  const brandWmis = wmiFor[brand];
  for await (const coc of expandGlob(join(USER_DIR, "*.pdf"))) {
    const vins = coc.name.match(vinRegEx);
    if (!vins) {
      continue;
    }
    const wmi = vins[0].substring(0, 3);
    if (brandWmis.includes(wmi)) {
      const cocPath = join(USER_DIR, coc.name);
      result.push(cocPath);
    }
  }
  return result;
}

async function splitToOldNewOrAll(cocArr: string[]) {
  const ALL = [...cocArr];
  const OLD = [];
  const NEW = [];
  if (SPLIT) {
    if (cocArr.length > 0) console.log("");
    for (const coc of cocArr) {
      writeAllSync(
        Deno.stdout,
        new TextEncoder().encode(basename(coc).replace(".pdf", "")),
      );
      const txtPath = await cocToTxt(coc, { silent: true });
      const fileContents = await Deno.readTextFile(txtPath);
      if (fileContents.includes("0.11.") && fileContents.includes("3.1.")) {
        NEW.push(coc);
        console.log(" --> NEW");
      } else {
        OLD.push(coc);
        console.log(" --> OLD");
      }
      await Deno.remove(txtPath);
      ALL.length = 0;
    }
  }
  return {
    ALL,
    OLD,
    NEW,
  };
}

if (import.meta.main) main();
