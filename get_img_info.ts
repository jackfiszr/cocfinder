import { SEARCH_DIR } from "./config.ts";
import {
  expandGlob,
  join,
  PDFDocument,
  PDFName,
  PDFRawStream,
} from "./deps.ts";

await walkDirs(SEARCH_DIR);

async function walkDirs(dir: string) {
  for await (const entry of Deno.readDir(dir)) {
    if (entry.isDirectory) {
      console.log("DIRECTORY:", entry.name);
      for await (const pdf of expandGlob(join(dir, entry.name, "*.pdf"))) {
        getImgDetails(pdf.path);
        break;
      }
      await walkDirs(join(dir, entry.name));
    }
  }
}

async function getImgDetails(originalPdfPath: string) {
  const pdfDoc = await PDFDocument.load(Deno.readFileSync(originalPdfPath));
  const enumeratedIndirectObjects = pdfDoc.context.enumerateIndirectObjects();
  const imagesInDoc = [];
  let objectIdx = 0;
  enumeratedIndirectObjects.forEach(([ref, pdfObject]) => {
    objectIdx += 1;
    if (!(pdfObject instanceof PDFRawStream)) return;

    const { dict } = pdfObject;

    const subtype = dict.get(PDFName.of("Subtype"));
    const width = dict.get(PDFName.of("Width"));
    const height = dict.get(PDFName.of("Height"));
    const name = dict.get(PDFName.of("Name"));
    const bitsPerComponent = dict.get(PDFName.of("BitsPerComponent"));

    if (subtype === PDFName.of("Image")) {
      imagesInDoc.push({
        ref,
        name: name ? name.key : `Object${objectIdx}`,
        width: width.numberValue,
        height: height.numberValue,
        bitsPerComponent: bitsPerComponent.numberValue,
        data: pdfObject.contents,
      });
    }
  });

  console.log("Images in PDF:");
  imagesInDoc.forEach((image) => {
    console.log(
      "Name:",
      image.name,
      "\n  Width:",
      image.width,
      "\n  Height:",
      image.height,
      "\n  Bits Per Component:",
      image.bitsPerComponent,
      "\n  Data:",
      `Uint8Array(${image.data.length})`,
      "\n  Ref:",
      image.ref.toString(),
    );
    console.log(image.data);
  });
  console.log();
}
