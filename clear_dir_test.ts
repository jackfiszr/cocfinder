import { assertEquals } from "./test_deps.ts";
import { main as clearDir } from "./clear_dir.ts";

Deno.test("removes pdf files from user dir", async () => {
  const res = await clearDir();
  console.log(res);
  assertEquals(res, res);
});
