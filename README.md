```
⌜                 ____________       _________
_____________________  __/__(_)____________  /____________
_  ___/  __ \  ___/_  /_ __  /__  __ \  __  /_  _ \_  ___/
/ /__ / /_/ / /__ _  __/ _  / _  / / / /_/ / /  __/  /
\___/ \____/\___/ /_/    /_/  /_/ /_/\__,_/  \___//_/
```

© 2020-2022 Jacek Fiszer, MIT License

Current stable version: v2.1.5

# Table of Contents

[TOC]

# Install Deno

Shell (Mac, Linux):

    curl -fsSL https://deno.land/x/install/install.sh | sh

PowerShell (Windows):

    iwr https://deno.land/x/install/install.ps1 -useb | iex

For more details go to [deno.land](https://deno.land/)

# Install the coc command

    deno install -f --check --allow-env --allow-read --allow-write --allow-run -n coc https://bitbucket.org/jackfiszr/cocfinder/raw/v2.1.5/mod.ts

or compile to binary:

    deno compile --check --allow-env --allow-read --allow-write --allow-run --output coc https://bitbucket.org/jackfiszr/cocfinder/raw/v2.1.5/mod.ts

# Generate fake input files

    coc mock --mock

# Split multipage pdf docs into separate 2-page docs

For this to work, you have to have a command line utility 'pdftotext' installed.
If you are on a Linux machine, then it is probably included by default as part
of poppler-utils package. If you are on Windows, put the file from bin folder of
this repository into your system path.

    coc split --mock [--check [30]]

Option `--check` validates earlier splits (and takes the number of days back as
an optional argument).

# Search for and copy specific docs

Create file `lista_vin.txt` with a list of vehicle identification numbers, e.g.:

    TMBZZZM0CKV1N0001
    TMBZZZM0CKV1N0002
    WVWZZZM0CKV1N0003
    WV1ZZZM0CKV1N0004
    WV2ZZZM0CKV1N0005
    WAUZZZM0CKV1N0006
    WUAZZZM0CKV1N0007
    TRUZZZM0CKV1N0008
    VSSZZZM0CKV1N0009
    VSSZZZM0CKV1N0010
    ...

Then run (from the same direrctory as the list file):

    coc find --mock --no-index

In the localization the script is executed from, a directory named with your
system username will be created, and all the found docs will be copied to it.

# Index the split docs

Indexing the docs allows for a much faster search time in case of very large
file sets

(ensure you have `index.txt` file in the split docs dir)

    coc index --mock

To use the file index while searching for docs omit the `--no-index` opt:

    coc find --mock

The command `coc splex` will split and then index the files (it takes all the
options and args as `split` and `index` do)

# Merge the copied docs

Will create a joined docs pdf, but separate for each brand:

    coc join [-s] [--hide-logo]

# Watermark the copied docs

Will create another copy of the copied docs, but with a 'DUPLICATE' mark:

    coc mark --text DUPLICATE --font-size 10 --pages odd --skew 0 --coord-x 750 --coord-y 35 --red 0.4 --green 0.5 --blue 0.6 --opacity 0.7

# Remove the copied docs

    coc clear

# Remove the split docs

    coc purge --mock

# Announce the result of the program operation by voice (Windows only)

When used with a `--speak` flag, most of the commands will report their results
using a speech synthesizer (this works only on Windows), e.g:

    coc find --mock --speak
