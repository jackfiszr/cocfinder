import {
  basename,
  copy,
  ensureDir,
  expandGlob,
  join,
  SEP,
  Spinner,
  swissKnife,
  Toggle,
} from "./deps.ts";
import {
  args,
  BRAND_DATA,
  COPY_LAST,
  SEARCH_DIR,
  USE_INDEX,
  USER_DIR,
  USER_INDEX_FILE,
  USERNAME,
  VIN_LIST_FILE,
} from "./config.ts";
import { dlog, syncIndex, timeDiff, txtToCleanArr } from "./utils.ts";

let INDEX_ARR: string[];

export async function main() {
  await ensureDir(USER_DIR);
  const vinList = await readVinList(VIN_LIST_FILE);
  if (vinList.length > 0) {
    await checkIfFilesExist(vinList);
    copyCocs(vinList);
  }
}

async function checkIfFilesExist(vinList: string[]) {
  if (vinList.length > 0) {
    let filesExist = false;
    for (const vin of vinList) {
      const globString = join("*", `*${vin}*.pdf`);
      for await (const file of expandGlob(globString)) {
        filesExist = true;
        const filepath = file.path;
        const username = filepath.split(SEP).reverse()[1];
        dlog({
          color: "red",
          title: vin,
          mainMsg: `Plik już istnieje w katalogu użytkownika ${username}`,
          subMsg: filepath,
        });
      }
    }
    if (filesExist) {
      if (args.speak) await swissKnife.winBeep();
      dlog({
        color: "bgRed",
        title: "KONTYNUOWAĆ?",
        mainMsg: "Powyższe pliki już wcześniej zostały skopiowane",
      });
      const continuation: boolean = await Toggle.prompt(
        "wciśnij: „n” i ENTER aby zakończyć / „y” i ENTER aby kontynuować",
      );
      if (continuation) console.log(" > kontynuacja...\n");
      else {
        console.log(" > skrypt zakończony przez użytkownika.\n");
        Deno.exit(0);
      }
    }
  }
}

function brandFromVin(vin: string) {
  const wmi = vin.substring(0, 3);
  for (const key in BRAND_DATA) {
    const wmis = BRAND_DATA[key]["WMI"];
    if (wmis.includes(wmi)) {
      const brandDir = BRAND_DATA[key]["OUT"];
      return brandDir;
    }
  }
  return false;
}

function isValidVin(vin: string) {
  let valid = true;
  if (vin.length !== 17) {
    valid = false;
    dlog({
      color: "red",
      title: vin,
      mainMsg: "BŁĘDNY VIN!",
      subMsg: `Numer ma długość ${vin.length} znaków`,
    });
  }
  const brand = brandFromVin(vin);
  if (!brand) {
    valid = false;
    dlog({
      color: "red",
      title: vin,
      mainMsg: "BŁĘDNY VIN?",
      subMsg: `Nie rozpoznano marki ${vin.substring(0, 3)}`,
    });
  }
  const vinNotAllowedLetters = ["I", "O", "Q"];
  vinNotAllowedLetters.forEach((letter) => {
    if (vin.toUpperCase().includes(letter)) {
      valid = false;
      dlog({
        color: "red",
        title: vin,
        mainMsg: "BŁĘDNY VIN!",
        subMsg: `Numer nie moze zawierać litery "${letter}"`,
      });
    }
  });
  return valid;
}

async function readVinList(fileName: string) {
  let vinListTxt = args._.splice(1).join("\n");
  let vinListArr: string[] = [];
  if (vinListTxt.length === 0) {
    try {
      vinListTxt = (await Deno.readTextFile(fileName)).trim();
    } catch (e) {
      dlog({
        color: "red",
        title: "BRAK LISTY",
        mainMsg: `Nie można otworzyć pliku ${fileName}`,
        subMsg: e,
      });
      return vinListArr;
    }
  }
  if (vinListTxt.length === 0) {
    dlog({
      color: "red",
      title: "BRAK VIN",
      mainMsg:
        `Podaj numery nadwozi w pliku ${fileName} lub jako argumenty polecenia coc find\n`,
    });
    return vinListArr;
  } else {
    dlog({
      color: "bgBlue",
      title: "ROZPOCZĘTO",
      mainMsg: "Wczytywanie listy VIN...",
      subMsg: new Date().toLocaleTimeString(),
    });
    vinListArr = txtToCleanArr(vinListTxt);
    const unverifiedVinsCount = vinListArr.length;
    vinListArr = vinListArr.filter(isValidVin);
    const verifiedVinsCount = vinListArr.length;
    const discardedVinsCount = unverifiedVinsCount - verifiedVinsCount;
    dlog({
      color: "blue",
      title: "WCZYTANO listę VIN",
      mainMsg: `${verifiedVinsCount} z ${unverifiedVinsCount} pozycji`,
      subMsg: `pozycji odrzucownych jako błędne: ${discardedVinsCount}\n`,
    });
  }
  return vinListArr;
}

async function findCocPaths(vin: string) {
  const results = [];
  if (isValidVin(vin)) {
    const brand = brandFromVin(vin);
    if (brand) {
      const globString = join(SEARCH_DIR, `${brand}*`, "**", `*${vin}*.pdf`);
      dlog({
        color: "yellow",
        title: vin,
        mainMsg: "ROZPOCZĘCIE WYSZUKIWANIA...",
        subMsg: globString,
      });
      if (USE_INDEX) {
        const match = INDEX_ARR.filter((l) => l.includes(vin)).map((l) =>
          l.trim()
        );
        if (match.length) {
          match.map((m) => join(SEARCH_DIR, join(...m.split("/"))));
          results.push(
            ...match.map((m) => join(SEARCH_DIR, join(...m.split("/")))),
          );
        }
      } else {
        for await (const el of expandGlob(globString)) {
          results.push(el.path);
        }
      }
    }
  }
  const isOld = (path: string) =>
    path.includes("_OLD") || path.includes("_przetworzone");
  const oldFirst = (a: string, b: string) => {
    if (isOld(a) && !isOld(b)) return -1;
    if (!isOld(a) && isOld(b)) return 1;
    return a.localeCompare(b);
  };
  results.sort(oldFirst);
  return results.reverse();
}

async function copyCocFile(vin: string) {
  let found = false;
  const cocPaths = await findCocPaths(vin);
  found = cocPaths.length > 0;
  if (found) {
    const filePath = cocPaths[0];
    const fileName = filePath.split(SEP).pop() || "";
    const fileCopy = join(USER_DIR, fileName);
    dlog({
      color: "green",
      title: vin,
      mainMsg: "...ZNALEZIONO...",
      subMsg: filePath,
    });
    await copy(filePath, fileCopy).then(() => {
      dlog({
        color: "green",
        title: vin,
        mainMsg: `...SKOPIOWANO DO KATALOGU ${USERNAME}.`,
        subMsg: fileCopy,
      });
    }).catch((e: Error) => {
      dlog({
        color: "red",
        title: vin,
        mainMsg: "BŁĄD!",
        subMsg: String(e),
      });
    });
  } else {
    dlog({
      color: "red",
      title: vin,
      mainMsg: "NIE ZNALEZIONO",
      subMsg:
        "VIN jest prawidłowy, ale brak pliku w przeszukiwanych katalogach",
    });
  }
  return found;
}

async function copyCocFiles(vin: string) {
  let found = false;
  const cocPaths = await findCocPaths(vin);
  const numberOfFiles = cocPaths.length;
  found = numberOfFiles > 0;
  if (found) {
    dlog({
      color: "green",
      title: vin,
      mainMsg: `...ZNALEZIONO ${numberOfFiles}...`,
      subMsg: cocPaths.join("\n > "),
    });
    while (cocPaths.length > 0) {
      const fileNumber = numberOfFiles - cocPaths.length + 1;
      const filePath = cocPaths.pop() || "";
      const fileName = filePath.split(SEP).pop() || "";
      const fileCopyName = fileName.includes("-") || numberOfFiles === 1
        ? fileName
        : fileName.replace(".pdf", `_${fileNumber}.pdf`);
      const fileCopy = join(USER_DIR, fileCopyName);
      await copy(filePath, fileCopy).then(() => {
        dlog({
          color: "green",
          title: vin,
          mainMsg: `...SKOPIOWANO DO KATALOGU ${USERNAME}.`,
          subMsg: fileCopy,
        });
      }).catch((e: Error) => {
        dlog({
          color: "red",
          title: vin,
          mainMsg: "BŁĄD!",
          subMsg: String(e),
        });
      });
    }
  } else {
    dlog({
      color: "red",
      title: vin,
      mainMsg: "NIE ZNALEZIONO",
      subMsg:
        "VIN jest prawidłowy, ale brak pliku w przeszukiwanych katalogach",
    });
  }
  return found;
}

async function copyCocs(vinList: string[]) {
  const startTime = Date.now();
  const results: boolean[] = [];
  if (USE_INDEX) {
    await syncIndex();
    const spinner = Spinner.getInstance();
    // spinner.setSpinnerType("line");
    spinner.start("wczytywanie indeksu plików...");
    INDEX_ARR = (await Deno.readTextFile(USER_INDEX_FILE)).split("\n");
    await spinner.succeed();
    console.log("\n");
  }
  const allVinsCount = vinList.length;
  for (const [i, vin] of vinList.entries()) {
    dlog({
      color: "inverse",
      title: `${i + 1} z ${allVinsCount}`,
      mainMsg: "",
    });
    let result;
    if (COPY_LAST) {
      result = await copyCocFile(vin);
      if (!result) {
        result = await copyScan(vin);
      }
    } else {
      const result1 = await copyCocFiles(vin);
      const result2 = await copyScan(vin);
      result = result1 || result2;
    }
    results.push(result);
  }
  const notFound = vinList.filter((_vin, index) => !results[index]);
  logEndResults(allVinsCount, notFound, startTime);
}

async function copyScan(vin: string) {
  let found = false;
  const globString = join(SEARCH_DIR, "skany", `*${vin}*.pdf`);
  for await (const scan of expandGlob(globString)) {
    const destination = join(
      USER_DIR,
      basename(scan.path).replace(".pdf", "__SKAN.pdf"),
    );
    try {
      await copy(scan.path, destination);
    } catch (e) {
      dlog({
        color: "yellow",
        title: vin,
        mainMsg: "BŁĄD KOPIOWANIA SKANU...",
        subMsg: String(e),
      });
    }
    found = true;
  }
  if (found) {
    dlog({
      color: "green",
      title: vin,
      mainMsg: "MAMY JEDNAK SKAN :)",
    });
  }
  return found;
}

function logEndResults(
  allVinsCount: number,
  notFound: string[],
  startTime: number,
) {
  const elapsedTime = timeDiff(startTime, Date.now());
  const notFoundCount = notFound.length;
  const foundCount = allVinsCount - notFoundCount;
  const endTime = new Date().toLocaleTimeString();
  if (foundCount) {
    if (args.speak) swissKnife.speak("Zabierz swoje C O C");
  } else {
    if (allVinsCount === 1) {
      if (args.speak) swissKnife.speak("Nie mamy pańskiego C O C");
    } else {
      if (args.speak) swissKnife.speak("Nie mamy pańskich C O C");
    }
  }
  if (notFound.length > 0) {
    dlog({
      color: "bgRed",
      title: `ZAKOŃCZONO`,
      mainMsg: `Znaleziono ${foundCount} z ${allVinsCount} CoC`,
      subMsg: `${endTime}\nponiższe ${notFoundCount} nie zostały znalezione:\n${
        notFound.join("\n")
      }`,
      subColor: "yellow",
    });
  } else if (foundCount === allVinsCount) {
    dlog({
      color: "bgGreen",
      title: `ZAKOŃCZONO`,
      mainMsg: `Znaleziono wszystkie wyszukiwane CoC`,
      subMsg: `${endTime}\nszukano: ${allVinsCount}, znaleziono: ${foundCount}`,
    });
  } else {
    if (args.speak) swissKnife.speak("Madafaka");
    dlog({
      color: "bgYellow",
      title: "STAŁO SIĘ COŚ BARDZO DZIWNEGO",
      mainMsg: `${foundCount} + ${notFoundCount} = ${allVinsCount} ??`,
      subMsg: endTime,
    });
  }
  console.log(
    `___________________________\nCzas wyszukiwania: ${elapsedTime}\n`,
  );
}

if (import.meta.main) main();
