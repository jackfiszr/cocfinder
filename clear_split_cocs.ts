import { expandGlob, join } from "./deps.ts";
import { args, MAIN_INDEX_FILE, os, SEARCH_DIR, TEMP_DIR } from "./config.ts";

export async function main() {
  if (os !== "linux" && !args.mock) {
    throw ("This should be run with --mock flag");
  }

  const globString = join(SEARCH_DIR, "*", "*");

  for await (const dir of expandGlob(globString)) {
    await Deno.remove(dir.path, { recursive: true });
    console.log(`rm ${dir.name}`);
  }

  try {
    await Deno.remove(TEMP_DIR, { recursive: true });
    console.log(`rm ${TEMP_DIR}`);
  } catch (e) {
    if (e.code !== "ENOENT") {
      console.error(e.code);
    }
  }

  await Deno.create(MAIN_INDEX_FILE);
}

if (import.meta.main) main();
