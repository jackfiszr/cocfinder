import {
  basename,
  copySync,
  degrees,
  dirname,
  ensureDirSync,
  join,
  PDFDocument,
  PDFPage,
  ProgressBar,
} from "./deps.ts";
import {
  args,
  BRAND_DATA,
  os,
  SEARCH_DIR,
  SOURCE_DIR,
  TEMP_DIR,
} from "./config.ts";
import { cocToTxt, dlog, isCompletedDir, vinRegEx } from "./utils.ts";

const INVALID_FILES: string[] = [];

export async function main(): Promise<void> {
  const EXCLUDED_NAMES = loadExcludedNames();
  const endResults: {
    [brand: string]: number;
  } = {};
  dlog({
    color: "bgBlue",
    title: "DZIELENIE",
    mainMsg:
      "CoC z nowych transferów zbiorczych plików pdf zostaną zapisane jako pojedyncze pliki",
    subMsg: `źródło: ${SOURCE_DIR}, zapis: ${SEARCH_DIR}, start: ${
      new Date().toLocaleTimeString()
    }`,
  });
  for (const brand in BRAND_DATA) {
    const outputDir = BRAND_DATA[brand]["OUT"];
    if (outputDir) {
      if (args["check"]) {
        await clearUnfinishedSplits(
          join(SEARCH_DIR, outputDir),
          args["check"],
        );
      }
      const newTransfers = [];
      const maxNumberOfStages = brand === "VW/AUDI" ? 2 : 1;
      const copiedDirs = listAll(SEARCH_DIR, outputDir);
      const inputDir = BRAND_DATA[brand]["PDF"];
      if (inputDir) {
        const inputPdfs = listAll(SOURCE_DIR, inputDir);
        const newPdfTransfers = inputPdfs.filter((name) =>
          !copiedDirs.includes(name.replace(".pdf", "")) &&
          !EXCLUDED_NAMES.includes(name)
        );
        const newPdfTransfersFilePaths = newPdfTransfers.map((name) =>
          join(SOURCE_DIR, inputDir, name)
        );
        newTransfers.push(...newPdfTransfersFilePaths);
      }
      const inputDirPcl = BRAND_DATA[brand]["PCL"];
      if (inputDirPcl) {
        const inputPcls = listAll(SOURCE_DIR, inputDirPcl);
        const newPclTraspfers = inputPcls.filter((name) =>
          !copiedDirs.includes(name) && !EXCLUDED_NAMES.includes(name)
        );
        const numberOfNewPcls = newPclTraspfers.length;
        dlog({
          color: "cyan",
          title: brand,
          mainMsg: `${numberOfNewPcls} nowych transferów PCL5`,
          subMsg: `źródło: ${inputDirPcl}, zapis: ${outputDir}, czas: ${
            new Date().toLocaleTimeString()
          }`,
        });
        if (numberOfNewPcls > 0) {
          const tempPdfsPaths = [];
          for (const pcl of newPclTraspfers) {
            const sourcePclPath = join(SOURCE_DIR, inputDirPcl, pcl);
            const unsplitPdfPath = join(TEMP_DIR, outputDir, pcl + ".pdf");
            const tempPdfPath = await pclToPdf(sourcePclPath, unsplitPdfPath);
            tempPdfsPaths.push(tempPdfPath);
          }
          newTransfers.unshift(...tempPdfsPaths);
        }
      }
      const numberOfNewTransfers = newTransfers.length;
      dlog({
        color: "green",
        title: brand,
        mainMsg: `${numberOfNewTransfers} nowych transferów zbiorczych`,
        subMsg: `źródło: ${inputDir}, zapis: ${outputDir}, czas: ${
          new Date().toLocaleTimeString()
        }`,
      });
      if (numberOfNewTransfers > 0) {
        for (const sourceFilePath of newTransfers) {
          const sourceFileName = basename(sourceFilePath);
          const tempPdfDirPath = join(
            TEMP_DIR,
            outputDir,
            sourceFileName.replace(".pdf", ""),
          );
          const splitPdfsPaths = await splitPdfEveryTwoPages(
            sourceFilePath,
            tempPdfDirPath,
            BRAND_DATA[brand].rotation,
          );
          const numberOfSplitPdfs = splitPdfsPaths.length;
          dlog({
            color: "inverse",
            title: brand,
            mainMsg:
              `Utworzono ${numberOfSplitPdfs} nowych podzielonych plików pdf`,
            subMsg: "następny krok: parsowanie numerów VIN...",
          });
          const txtFilePaths = [];
          for (const splitPdfPath of splitPdfsPaths) {
            try {
              const txtFilePath = await cocToTxt(splitPdfPath);
              txtFilePaths.push(txtFilePath);
            } catch (e) {
              dlog({
                color: "red",
                title: "BŁĄD PRZY KONWERTOWANIU DO TXT",
                mainMsg: splitPdfPath,
                subMsg: e,
              });
              Deno.exit(1);
            }
          }
          const numberOfTxtFiles = txtFilePaths.length;
          dlog({
            color: "italic",
            title: brand,
            mainMsg:
              `Konwertowanie ${numberOfTxtFiles} plików do txt zakończone`,
            subMsg: "następny krok: zmiana nazwy utworzonych plików pdf",
          });
          const createdCocsPaths = [];
          const outputDirPath = tempPdfDirPath.replace(TEMP_DIR, SEARCH_DIR);
          const wipFile = join(outputDirPath, ".WIP");
          try {
            Deno.mkdirSync(outputDirPath);
            Deno.createSync(wipFile);
          } catch (e) {
            dlog({
              color: "red",
              title: "BŁĄD",
              mainMsg: `Nie można utworzyć katalogu ${outputDirPath}`,
              subMsg: e,
            });
            Deno.exit(1);
          }
          dlog({
            color: "yellow",
            title: "UTWORZONO",
            mainMsg: outputDirPath,
            subMsg:
              "nowy katalog, w którym zapisane zostaną podzielone pliki pdf",
          });
          for (const splitPdfPath of splitPdfsPaths) {
            const createdCocPath = await renamePdfUsingVin(
              splitPdfPath,
              maxNumberOfStages,
            );
            createdCocsPaths.push(createdCocPath);
          }
          const numberOfCreatedCocs = createdCocsPaths.length;
          dlog({
            color: "bgGreen",
            title: brand,
            mainMsg: `Dodano ${numberOfCreatedCocs} nowych dokumentów CoC`,
            subMsg: "dokumenty są już dostępne dla skryptu wyszukującego",
          });
          if (endResults[brand]) {
            endResults[brand] += numberOfCreatedCocs;
          } else {
            endResults[brand] = numberOfCreatedCocs;
          }
          const numberOfFilesInDir =
            Array.from(Deno.readDirSync(outputDirPath)).length - 1;
          if (
            numberOfFilesInDir === numberOfCreatedCocs ||
            outputDirPath.includes("PCL5")
          ) {
            Deno.removeSync(wipFile);
          } else {
            dlog({
              color: "red",
              title: "Dzielenie niekompletne",
              mainMsg:
                `katalog powiniem zawierać ${numberOfCreatedCocs} pliki, a zawiera ${numberOfFilesInDir}`,
              subMsg: outputDirPath,
            });
            Deno.exit();
          }
        }
      }
    }
  }
  dlog({
    color: "magenta",
    title: "RAPORT",
    mainMsg: "liczba dodanych CoC",
    subMsg: JSON.stringify(endResults, null, 2),
  });
  if (INVALID_FILES.length > 0) {
    dlog({
      color: "bgMagenta",
      title: "RAPORT",
      mainMsg: "błędne transfery",
      subMsg: `poniższych plików nie można było podzielić:\n${
        INVALID_FILES.join("\n")
      }`,
    });
  }
}

async function splitPdfEveryTwoPages(
  sourceFilePath: string,
  outputDirPath: string,
  rotation?: number,
): Promise<string[]> {
  const createdFilesPaths: string[] = [];
  try {
    const sourceFile = await PDFDocument.load(
      Deno.readFileSync(sourceFilePath),
    );
    const byteArrays: Uint8Array[] = await sourceFile.getPages().reduce(
      async (prevResult: Promise<Uint8Array[]>, _page: PDFPage, i: number) => {
        const result = await prevResult;
        if (i % 2 === 0) {
          const doc = await PDFDocument.create();
          const pages = await doc.copyPages(sourceFile, [i, i + 1]);
          pages.forEach((page: PDFPage) => {
            const { width, height } = page.getSize();
            const rotationAngle = page.getRotation().angle;
            if (rotation && rotationAngle === 0 && width < height) {
              page.setRotation(degrees(rotation));
            }
            doc.addPage(page);
          });
          result.push(await doc.save());
        }
        return result;
      },
      Promise.resolve([]),
    );
    dlog({
      color: "yellow",
      title: "DZIELENIE",
      mainMsg: sourceFilePath,
      subMsg: `zawiera ${byteArrays.length} dokumentów CoC`,
    });
    const filename = basename(sourceFilePath).replace(".pdf", "");
    byteArrays.forEach((pdfBytes, i) => {
      ensureDirSync(outputDirPath);
      const outputFilePath = join(
        outputDirPath,
        `./${filename}-${i + 1}.pdf`,
      );
      Deno.writeFileSync(outputFilePath, pdfBytes);
      dlog({
        color: "gray",
        title: "UTWORZONO",
        mainMsg: outputFilePath,
      });
      createdFilesPaths.push(outputFilePath);
    });
  } catch (error) {
    dlog({
      color: "red",
      title: "DZIELENIE",
      mainMsg: sourceFilePath,
      subMsg: error,
    });
    Deno.exit(1);
  }
  if (createdFilesPaths.length === 0) {
    INVALID_FILES.push(basename(sourceFilePath));
  }
  return createdFilesPaths;
}

function renamePdfUsingVin(
  pdfFilePath: string,
  docCopies?: number,
): string | undefined {
  if (!docCopies) {
    docCopies = 2;
  }
  if (docCopies > 2) {
    throw ("Currently a maximum of 2 CoCs for the same VIN per transfer is allowed.");
  }
  let vinFilePath = pdfFilePath.replace(TEMP_DIR, SEARCH_DIR);
  try {
    const fileName = basename(pdfFilePath);
    const vin = extractVin(pdfFilePath.replace(".pdf", ".txt"));
    vinFilePath = vinFilePath.replace(fileName, `${vin}.pdf`);
    copySync(pdfFilePath, vinFilePath);
    dlog({
      color: "green",
      title: "UTWORZONO",
      mainMsg: vinFilePath,
      subMsg: pdfFilePath,
    });
    return vinFilePath;
  } catch (e) {
    if (e.message.includes("already exists")) {
      if (docCopies === 1) {
        return;
      }
      try {
        Deno.renameSync(vinFilePath, vinFilePath.replace(".pdf", "-1.pdf"));
        vinFilePath = vinFilePath.replace(".pdf", "-2.pdf");
        copySync(pdfFilePath, vinFilePath);
      } catch (e) {
        console.error(`Cannot create stage ${docCopies} CoC: ${e}`);
        Deno.exit(1);
      }
    } else {
      console.error(e);
      Deno.exit(1);
    }
  }
}

function extractVin(txtFilePath: string): string {
  let vins;
  try {
    const fileContents = Deno.readTextFileSync(txtFilePath);
    vins = fileContents.match(vinRegEx);
  } catch (e) {
    console.error(e);
    Deno.exit(1);
  }
  if (!vins) {
    throw (`VIN not found in file ${txtFilePath}`);
  }
  return vins[0];
}

function listAll(baseDir: string, brandDir: string) {
  const result = [];
  const dirPath = join(baseDir, brandDir);
  for (const path of Deno.readDirSync(dirPath)) {
    result.push(path.name);
  }
  return result;
}

function loadExcludedNames(): string[] {
  const excludedListFilePath = join(SEARCH_DIR, "exclude.txt");
  try {
    return Deno.readTextFileSync(excludedListFilePath).split("\n").map((name) =>
      name.trim()
    ).filter((name) => name.length > 0);
  } catch (error) {
    dlog({
      color: "yellow",
      title: "POMIJANIE",
      mainMsg:
        `w celu wykluczenia plików lub folderów należy zapisać ich nazwy w pliku ${excludedListFilePath}`,
      subMsg: error,
    });
    return [];
  }
}

async function pclToPdf(
  pclFilePath: string,
  pdfFilePath: string,
): Promise<string> {
  const programName = "WinPCLtoPDF";
  let title = "JUŻ ISTNIEJE";
  ensureDirSync(dirname(pdfFilePath));
  let runOptions: Deno.RunOptions;
  if (os === "windows") {
    runOptions = {
      cmd: [programName, pclFilePath, pdfFilePath, "silent", "batch"],
    };
  } else {
    runOptions = {
      cmd: [
        "wine",
        `./bin/${programName}.exe`,
        pclFilePath,
        pdfFilePath,
        "silent",
        "batch",
      ],
    };
  }
  try {
    const process = Deno.run(runOptions);
    await process.status();
    process.close();
  } catch (error) {
    dlog({
      color: "red",
      title: "BŁĄD",
      mainMsg: `Nie można przekonwertować pliku ${pclFilePath} do pliku pdf`,
      subMsg:
        `${error} — upewnij się, że program ${programName} jest zainstalowany`,
    });
    Deno.exit(1);
  }
  title = "UTWORZONO";
  dlog({
    color: "italic",
    title: title,
    mainMsg: pdfFilePath,
  });
  return pdfFilePath;
}

function clearUnfinishedSplits(parentDir: string, numberOfDaysBack?: unknown) {
  const now = Date.now();
  let dirsToCheck = Array.from(Deno.readDirSync(parentDir)).filter((el) =>
    el.isDirectory
  ).map((dir) => dir.name);
  let total = dirsToCheck.length;

  if (Number.isFinite(numberOfDaysBack)) {
    console.log(
      `\nSzukam CoC z ostatnich ${numberOfDaysBack} dni w ${parentDir}`,
    );
    const filterProgress = new ProgressBar({
      total,
      width: 50,
    });
    let filterCompleted = 0;
    const millisecondsBack = Number(numberOfDaysBack) * 86400000;
    const newDirs = dirsToCheck.filter((dir) => {
      if (filterCompleted <= total) {
        filterProgress.render(filterCompleted++);
      }
      const birthtime = Deno.statSync(join(parentDir, dir)).birthtime;
      const craetedAt = new Date(`${birthtime}`).getTime();
      return now - craetedAt < millisecondsBack;
    });
    dirsToCheck = newDirs;
    total = newDirs.length;
    console.log();
  }

  console.log(`\nSprawdzam integralność danych w ${parentDir}`);

  const checkProgress = new ProgressBar({
    total,
    width: 50,
  });
  let checkCompleted = 0;

  const removedDirs = [];
  for (const dir of dirsToCheck) {
    const childDir = join(parentDir, dir);
    if (!isCompletedDir(childDir)) {
      Deno.removeSync(childDir, { recursive: true });
      removedDirs.push(childDir);
    }
    if (checkCompleted <= total) {
      checkProgress.render(checkCompleted++);
    }
  }

  console.log();

  if (removedDirs.length) {
    dlog({
      color: "bgBrightMagenta",
      title: "WYKRYTO NIEDOKOŃCZONE WCZEŚNIEJSZE DZIELENIA",
      mainMsg: "USUNIĘTO NIEKOMPLETNE KATALOGI COC",
      subMsg: removedDirs.join("\n > "),
    });
  }
}

if (import.meta.main) await main();
