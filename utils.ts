import { DISK_MAPPINGS, MAIN_INDEX_FILE, USER_INDEX_FILE } from "./config.ts";
import {
  appVer,
  copy,
  ensureFile,
  join,
  PDFPage,
  rgb,
  Spinner,
} from "./deps.ts";
export {
  dlog,
  pdfToTxt as cocToTxt,
  timeDiff,
  txtToCleanArr,
  verCheck,
} from "https://bitbucket.org/jackfiszr/utils/raw/v1.3.8/mod.ts";

export function intro() {
  const introStr = `
                    ____________       _________
  _____________________  __/__(_)____________  /____________
  _  ___/  __ \\  ___/_  /_ __  /__  __ \\  __  /_  _ \\_  ___/
  / /__ / /_/ / /__ _  __/ _  / _  / / / /_/ / /  __/  /
  \\___/ \\____/\\___/ /_/    /_/  /_/ /_/\\__,_/  \\___//_/ ${appVer}
`;
  console.log(introStr);
}

export const wmiFor: {
  [brandName: string]: string[];
} = {
  AUDI: ["WAU", "WUA", "TRU"],
  PORSCHE: ["WP0", "WP1"],
  SEAT: ["VSS"],
  SKODA: ["TMB"],
  VW: ["WVW", "WVG", "WV1", "WV2", "WV3", "WV4"],
};

export const brandNames = Object.keys(wmiFor);

const allWmis = Object.values(wmiFor).flat();

export const vinRegEx = new RegExp(`(${allWmis.join("|")})\\w{14}`, "g"); // /(TMB|WVW|WVG|WV1|WV2|WV3|WAU|WUA|TRU|VSS)\w{14}/g

export function mapDisk(
  uncPath: string,
  diskLetter: keyof typeof DISK_MAPPINGS,
) {
  return uncPath.replace(DISK_MAPPINGS[diskLetter], `${diskLetter}:`);
}

export async function syncIndex() {
  const spinner = Spinner.getInstance();
  // spinner.setSpinnerType("line");
  try {
    const mainIndexMtime = (await Deno.stat(MAIN_INDEX_FILE)).mtime;
    const userIndexMtime = (await Deno.stat(USER_INDEX_FILE)).mtime;
    if (
      mainIndexMtime && userIndexMtime &&
      mainIndexMtime > userIndexMtime
    ) {
      spinner.start("aktualizowanie lokalnego indeksu plików...");
      await copy(MAIN_INDEX_FILE, USER_INDEX_FILE, { overwrite: true });
      await spinner.succeed();
      console.log("\n");
    }
  } catch (e) {
    if (e.name === "NotFound" && e.message.includes(USER_INDEX_FILE)) {
      await ensureFile(USER_INDEX_FILE);
      const oldDate = new Date(1970, 0, 1);
      await Deno.utime(USER_INDEX_FILE, oldDate, oldDate);
      await syncIndex();
    } else {
      console.error(e);
      Deno.exit(1);
    }
  }
}

export async function isEmptyDir(path: string) {
  for await (const _el of Deno.readDir(path)) {
    return false;
  }
  return true;
}

export function isCompletedDir(dirPath: string) {
  try {
    Deno.openSync(join(dirPath, ".WIP"));
    return false;
  } catch {
    return true;
  }
}

export function drawUpperRightRect(page: PDFPage) {
  const { width: pageWidth, height: pageHeight } = page.getSize();
  let patchWidth = 100;
  let patchHeight = 90;
  let patchX = pageWidth - patchWidth;
  let patchY = pageHeight - patchHeight;
  const rotationAngle = page.getRotation().angle;
  if (rotationAngle === 0) {
    page.drawRectangle({
      x: pageWidth / 2,
      y: pageHeight - 20,
      width: pageWidth / 2,
      height: 20,
      color: rgb(1, 1, 1),
      // opacity: 0.5,
    });
  } else if (rotationAngle === 90) {
    [patchWidth, patchHeight] = [patchHeight, patchWidth];
    [patchX, patchY] = [0, pageHeight - patchHeight];
  } else if (rotationAngle === 270) {
    [patchWidth, patchHeight] = [patchHeight, patchWidth];
    [patchX, patchY] = [pageWidth - patchWidth, 0];
  }
  page.drawRectangle({
    x: patchX,
    y: patchY,
    width: patchWidth,
    height: patchHeight,
    color: rgb(1, 1, 1),
    // opacity: 0.5,
  });
}

export function transformCoordsByRotation(
  rotationAngle: number,
  coordsAndPageSize: {
    x: number;
    y: number;
    width: number;
    height: number;
  },
) {
  switch (rotationAngle) {
    case 90:
      [coordsAndPageSize.width, coordsAndPageSize.height] = [
        coordsAndPageSize.height,
        coordsAndPageSize.width,
      ];
      [coordsAndPageSize.x, coordsAndPageSize.y] = [
        coordsAndPageSize.width - coordsAndPageSize.y,
        coordsAndPageSize.x,
      ];
      break;
    case 180:
      [coordsAndPageSize.x, coordsAndPageSize.y] = [
        coordsAndPageSize.width - coordsAndPageSize.x,
        coordsAndPageSize.height - coordsAndPageSize.y,
      ];
      break;
    case 270:
      [coordsAndPageSize.width, coordsAndPageSize.height] = [
        coordsAndPageSize.height,
        coordsAndPageSize.width,
      ];
      [coordsAndPageSize.x, coordsAndPageSize.y] = [
        coordsAndPageSize.y,
        coordsAndPageSize.height - coordsAndPageSize.x,
      ];
      break;
  }
  return coordsAndPageSize;
}

export function toRadians(angle: number) {
  return angle * Math.PI / 180;
}
