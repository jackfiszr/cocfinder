import {
  copy,
  cow,
  fmt,
  join,
  ProgressBar,
  SEP,
  Spinner,
  swissKnife,
} from "./deps.ts";
import {
  args,
  BRAND_DIRS,
  MAIN_INDEX_FILE,
  SEARCH_DIR,
  USER_INDEX_FILE,
} from "./config.ts";
import { dlog, isCompletedDir, isEmptyDir, syncIndex } from "./utils.ts";

export async function main() {
  const allDirs: string[] = [];
  const newDirs: string[] = [];
  const newFiles: string[] = [];

  dlog({
    color: "bgBlue",
    title: "INDEKSOWANIE",
    mainMsg: "Dodawanie nowych plików do listy",
    subMsg: new Date().toLocaleTimeString() + ", proszę czekać...\n",
  });

  await syncIndex();
  const spinner1 = Spinner.getInstance();
  // spinner1.setSpinnerType("line");
  spinner1.start("wczytywanie indeksu plików...");
  const INDEX_STR = await Deno.readTextFile(USER_INDEX_FILE);
  await spinner1.succeed();
  console.log("\n");

  const spinner2 = Spinner.getInstance();
  // spinner2.setSpinnerType("line");
  spinner2.start("listowanie katalogów...");
  for (const brandDir of BRAND_DIRS) {
    const brandDirPath = join(SEARCH_DIR, brandDir);
    for await (const el of Deno.readDir(brandDirPath)) {
      if (el.isDirectory) {
        allDirs.push([brandDir, el.name].join("/"));
      }
    }
  }
  await spinner2.succeed();
  console.log("\n");

  // const spinner3 = Spinner.getInstance();
  // spinner3.setSpinnerType("line");
  // spinner3.start("wykrywanie nowych katalogów...");
  const title = "wykrywanie nowych katalogów:";
  const total = allDirs.length;
  const progress = new ProgressBar({
    title,
    total,
    // here ==>
    width: 20,
    // width: 1000, // longer than the terminal width
    // <== here
  });
  let completed = 0;
  for (const dir of allDirs) {
    if (!INDEX_STR.includes(dir)) {
      const dirPath = join(SEARCH_DIR, ...dir.split("/"));
      if (isCompletedDir(dirPath)) newDirs.push(dir);
    }
    if (completed <= total) {
      progress.render(completed++);
    }
  }
  // await spinner3.succeed();
  console.log("\n");

  if (newDirs.length === 0) {
    console.log(fmt.bold(fmt.yellow("\nNA DYSKU NIE BYŁO NOWYCH FOLDERÓW")));
  } else {
    const spinner4 = Spinner.getInstance();
    // spinner4.setSpinnerType("line");
    spinner4.start("listowanie nowych plików...");
    for (const dir of newDirs) {
      const path = join(SEARCH_DIR, dir.split("/").join(SEP));
      if (await isEmptyDir(path)) {
        console.log(fmt.bold(fmt.red(`katalog ${path} jest pusty\n`)));
      }
      for await (const el of Deno.readDir(path)) {
        if (el.isFile && el.name !== ".WIP") { // do not include .WIP files (they are supposed to be already removed by now, but for some reason still appear in the readDir output)
          newFiles.push(fmtPath(join(path, el.name)));
        }
      }
    }
    await spinner4.succeed();
    console.log("\n");
  }

  if (newFiles.length > 0) {
    const spinner5 = Spinner.getInstance();
    // spinner5.setSpinnerType("line");
    spinner5.start("dodawanie do indeksu...");
    const result = newFiles.join("\n") + "\n";
    const encoder = new TextEncoder();
    const ecoded = encoder.encode(result);
    await Deno.writeFile(USER_INDEX_FILE, ecoded, { append: true });
    await spinner5.succeed();
    console.log("\n");
    const spinner6 = Spinner.getInstance();
    // spinner6.setSpinnerType("line");
    spinner6.start("aktualizowanie indeksu głównego...");
    await copy(USER_INDEX_FILE, MAIN_INDEX_FILE, { overwrite: true });
    await spinner6.succeed();
    console.log("\n");
    dlog({
      color: "bgGreen",
      title: "DOPISANO",
      mainMsg: "",
      subPrefix: "\n",
      subMsg: result,
    });
    if (args.speak) swissKnife.speak("Dodałam wszyściutko");
    const msg = cow.say({
      text: "GREAT SUCCESS!",
      mode: Math.floor(Math.random() * 9),
      random: Math.floor(Math.random() * 10) === 9,
    });
    console.log(fmt.bold(fmt.green(msg)));
  } else {
    if (args.speak) swissKnife.speak("Niec niewejszło");
    console.log(" > brak nowych plików");
  }
}

function fmtPath(path: string) {
  return path.replace(SEARCH_DIR + SEP, "").split(SEP).join("/");
}

if (import.meta.main) main();
