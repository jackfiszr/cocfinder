import { expandGlob, join, PDFDocument, swissKnife } from "./deps.ts";
import { args, USER_DIR } from "./config.ts";
import { drawUpperRightRect } from "./utils.ts";

export async function main() {
  const globString = join(USER_DIR, "*.pdf");
  let anythingDone = false;

  for await (const pdf of expandGlob(globString)) {
    const suffix = "_SCHOWANE_LOGO";
    if (!pdf.name.includes(suffix)) {
      const pdfBytes = await hideLogo(pdf.path);
      const filepath = pdf.path.replace(".pdf", `${suffix}.pdf`);
      await Deno.writeFile(filepath, pdfBytes);
      console.log(`Utworzono ${filepath}`);
      await Deno.remove(pdf.path);
      anythingDone = true;
    }
  }
  if (args.speak) {
    if (anythingDone) swissKnife.speak("Wszysiciutko pozasuaniaua");
    else swissKnife.speak("Nic nie by O", { rate: -1 });
  }

  async function hideLogo(pdfPath: string) {
    const existingPdfBytes = await Deno.readFile(pdfPath);
    const pdfDoc = await PDFDocument.load(existingPdfBytes);
    const pages = pdfDoc.getPages();
    pages.forEach((page, i) => {
      if (i % 2 === 0) {
        drawUpperRightRect(page);
      }
    });
    return await pdfDoc.save();
  }
}

if (import.meta.main) main();
