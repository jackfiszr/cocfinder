import { args, USER_DIR } from "./config.ts";
import {
  degrees,
  expandGlob,
  join,
  PDFDocument,
  rgb,
  swissKnife,
} from "./deps.ts";
import { toRadians, transformCoordsByRotation } from "./utils.ts";

export async function main() {
  const globString = join(USER_DIR, "*.pdf");

  for await (const pdf of expandGlob(globString)) {
    let text = "DUPLIKAT";
    if (args["text"] !== undefined) text = String(args["text"]);
    else if (args["t"] !== undefined) text = String(args["t"]);
    const filenameSuffix = `[${text}].pdf`;
    if (!pdf.name.includes(filenameSuffix)) {
      const pdfBytes = await markCoc(pdf.path, text);
      const filepath = pdf.path.replace(".pdf", filenameSuffix);
      Deno.writeFileSync(filepath, pdfBytes);
      console.log(`Utworzono ${filepath}`);
    }
  }
  if (args.speak) swissKnife.speak("Wszyściutko podstemplowałam");

  async function markCoc(pdfPath: string, text: string) {
    const existingPdfBytes = Deno.readFileSync(pdfPath);
    const pdfDoc = await PDFDocument.load(existingPdfBytes);
    const helveticaFont = await pdfDoc.embedFont("Helvetica");
    const fontSize = args["font-size"] as number || args["f"] as number || 10;
    const pages = pdfDoc.getPages();
    const pagesArg = args["pages"] || args["p"];
    pages.forEach((page, i) => {
      if (pagesToMark(String(pagesArg), pages.length).includes(i)) {
        const rotationAngle = page.getRotation().angle;
        let { width, height } = page.getSize();
        if (rotationAngle % 180 !== 0) {
          [width, height] = [height, width];
        }
        const textWidth = helveticaFont.widthOfTextAtSize(text, fontSize);
        const rotate = args["skew"] as number || args["s"] as number || 0;
        const textShiftX = textWidth * Math.cos(toRadians(rotate)) / 2;
        const textShiftY = textWidth * Math.sin(toRadians(rotate)) / 2;
        const x = args["x"] as number || args["coord-x"] as number ||
          width / 2 - textShiftX;
        const y = args["y"] as number || args["coord-y"] as number ||
          height / 2 - textShiftY;
        const coords = transformCoordsByRotation(rotationAngle, {
          x,
          y,
          width,
          height,
        });
        const red = args["red"] as number || args["r"] as number || 0;
        const green = args["green"] as number || args["g"] as number || 0;
        const blue = args["blue"] as number || args["b"] as number || 0;
        page.drawText(text, {
          x: coords.x,
          y: coords.y,
          size: fontSize,
          font: helveticaFont,
          color: rgb(red, green, blue),
          rotate: degrees(rotate + rotationAngle),
          opacity: args["opacity"] as number || args["o"] as number || 0.5,
        });
      }
    });
    return await pdfDoc.save();
  }
}

function pagesToMark(pagesArg: string, totalPages: number) {
  let result = [...Array(totalPages).keys()];
  if (pagesArg === "odd") {
    result = result.filter((number) => number % 2 === 0);
  } else if (pagesArg === "even") {
    result = result.filter((number) => number % 2 === 1);
  } else if (pagesArg !== "undefined") {
    result = pagesArg.split(",").map((el) => Number(el) - 1);
  }
  return result;
}

if (import.meta.main) main();
